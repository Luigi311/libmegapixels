project('libmegapixels', 'c',
    version: '0.1.0',
    license: 'GPL',
)

libconfig = dependency('libconfig')

# We use libtool-version numbers because it's easier to understand.
# Before making a release, the libmegapixels_so_*
# numbers should be modified. The components are of the form C:R:A.
# a) If binary compatibility has been broken (eg removed or changed interfaces)
#    change to C+1:0:0.
# b) If interfaces have been changed or added, but binary compatibility has
#    been preserved, change to C+1:0:A+1
# c) If the interface is the same as the previous version, change to C:R+1:A
libmegapixels_lt_c=1
libmegapixels_lt_r=0
libmegapixels_lt_a=0

libmegapixels_so_version = '@0@.@1@.@2@'.format((libmegapixels_lt_c - libmegapixels_lt_a),
                                              libmegapixels_lt_a,
                                              libmegapixels_lt_r)

inc = include_directories('include')
install_headers('include/libmegapixels.h')

lib_src = [
    'src/findconfig.c',
    'src/log.c',
    'src/mode.c',
    'src/util.c',
    'src/parse.c',
    'src/pipeline.c',
    'src/aaa.c',
]
libmegapixels = shared_library('megapixels', lib_src,
    version: libmegapixels_so_version,
    include_directories: inc,
    dependencies: libconfig,
    install: true
)

pkg_mod = import('pkgconfig')
pkg_mod.generate(libraries: libmegapixels,
                version: libmegapixels_so_version,
                name: 'libmegapixels',
                filebase: 'libmegapixels',
                description: 'The camera control bits from Megapixels')

conf = configuration_data()
conf.set_quoted('DATADIR', join_paths(get_option('prefix'), get_option('datadir')))
conf.set_quoted('SYSCONFDIR', get_option('sysconfdir'))
configure_file(
  output: 'config.h',
  configuration: conf)

executable('megapixels-findconfig', 'util/findconfig.c',
    link_with: libmegapixels,
    include_directories: inc,
    install: true,
    )
executable('megapixels-getframe', 'util/getframe.c',
    link_with: libmegapixels,
    include_directories: inc,
    install: true,
    )

cc = meson.get_compiler('c')
m_dep = cc.find_library('m', required : false)
executable('megapixels-sensorprofile', 'util/sensorprofile.c',
    link_with: libmegapixels,
    include_directories: inc,
    install: true,
    dependencies: m_dep
    )


install_data(
    [
        'config/pine64,pinephone.conf',
        'config/pine64,pinephone-pro.conf',
        'config/pine64,pinetab.conf',
        'config/purism,librem5.conf',
        'config/xiaomi,scorpio.conf',
    ],
    install_dir: get_option('datadir') / 'megapixels/config/'
)
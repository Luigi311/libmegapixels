#include <stdint.h>
#include "libmegapixels.h"

#pragma once

struct libmegapixels_modename {
		char *name;
		uint32_t v4l_pixel_format;
		uint32_t media_bus_format;
		int bpp;
		int bpc;
		int cfa;
};

int
libmegapixels_v4l_pixfmt_to_index(uint32_t pixfmt);

int
libmegapixels_format_name_to_index(const char *name);

int
mode_snprintf(char *buf, size_t maxlen, libmegapixels_mode *mode);